#ifndef LGIN_H
#define LGIN_H

#include <QWidget>

namespace Ui {
class Lgin;
}

class Lgin : public QWidget
{
    Q_OBJECT

public:
    explicit Lgin(QWidget *parent = 0);
    ~Lgin();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_Buttonjuli_clicked();


    void on_Buttonzhuzhou_clicked();

    void on_Buttontui_1_clicked();

    void on_Buttontui_2_clicked();

signals:
    void color();
    void zhuzhou();
    void tuizi1();
    void tuizi2();


private:
    Ui::Lgin *ui;
};

#endif // LGIN_H
