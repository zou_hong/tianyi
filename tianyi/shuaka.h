#ifndef SHUAKA_H
#define SHUAKA_H

#include <QDialog>
#include <QMediaPlayer>

namespace Ui {
class SHUAKA;
}

class SHUAKA : public QDialog
{
    Q_OBJECT

public:
    explicit SHUAKA(QWidget *parent = 0);
    ~SHUAKA();

private slots:
    void on_Buttonsucc_clicked();

    void on_pushButton_clicked();

    void send();

signals:
    void opensignal();

    void shuaka();




private:
    Ui::SHUAKA *ui;

};

#endif // SHUAKA_H
