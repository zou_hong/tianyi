#-------------------------------------------------
#
# Project created by QtCreator 2018-04-25T14:13:51
#
#-------------------------------------------------

QT       += core gui network
CONFIG   += resources_big
QT       +=multimedia
QT       += webkitwidgets
QT       += serialport


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TianYi
TEMPLATE = app


SOURCES += main.cpp \
    welcome.cpp \
    shizhong.cpp \
    shuaka.cpp \
    zhu.cpp \
    lgin.cpp \
    html.cpp \
    fankui.cpp

HEADERS  += \
    welcome.h \
    shizhong.h \
    shuaka.h \
    zhu.h \
    lgin.h \
    html.h \
    fankui.h

FORMS    += widget.ui \
    shizhong.ui \
    shuaka.ui \
    zhu.ui \
    lgin.ui \
    html.ui \
    fankui.ui

RESOURCES += \
    image.qrc \
    music.qrc \
    shipin.qrc \
    html.qrc

DISTFILES += \
    tianyi.rc
RC_FILE = \
    tianyi.rc
