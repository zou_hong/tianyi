#ifndef HTML_H
#define HTML_H

#include <QWidget>

namespace Ui {
class Html;
}

class Html : public QWidget
{
    Q_OBJECT

public:
    explicit Html(QWidget *parent = 0);
    ~Html();

private:
    Ui::Html *ui;
};

#endif // HTML_H
