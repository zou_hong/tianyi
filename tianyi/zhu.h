#ifndef ZHU_H
#define ZHU_H

#include <QMainWindow>
#include<QWidget>
#include"welcome.h"
#include <QSerialPort>
#include<QSerialPortInfo>
#include <QTimer>
#include<QStandardItem>
#include <QMessageBox>
#include<QMediaPlayer>

namespace Ui {
class ZHU;
}

class ZHU : public QMainWindow
{
    Q_OBJECT

public:
    explicit ZHU(QWidget *parent = 0);
    ~ZHU();
    void initPort();//串口


protected:
    //绘图事件
    void paintEvent(QPaintEvent*);
    void sleep(unsigned int msec);
    int type = 0;
    int amount;
    int i=0,j=0;
    int guoming=0;
    int n=2;


private slots:
    void on_Buttonfood_clicked();

    void on_Buttondrink_clicked();

    void on_Buttonguomin_clicked();

    void on_pushButton1_clicked();

    void on_Buttonkele_clicked();

    void on_pushButton2_clicked();

    void on_pushButton3_clicked();

    void on_pushButton4_clicked();

    void on_pushButton5_clicked();

    void on_pushButton6_clicked();

    void on_pushButton7_clicked();

    void on_pushButton8_clicked();

    void on_pushButton9_clicked();

    void on_pushButton10_clicked();

    void on_pushButton11_clicked();

    void on_pushButton12_clicked();

    void on_Buttonjixu_clicked();

    void on_pushButton_clicked();

    void dealch();

    void dealen();

    void dealopen();

    void on_Buttonback_clicked();

    void jiance();

    //维护检测
    void juli_color();

    void zhuzhou();

    void tuizi1();

    void tuizi2();


    /*********串口通信*****/
    void readComDataSlot();
    void StringToHex(QString str, QByteArray & senddata);
    char ConvertHexChar(char ch);


    void on_btnOpen_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::ZHU *ui;
    Widget *welcome =new Widget;
    //通信
    QTimer *timer_2;
    QTimer *timer;
    QSerialPort *my_serialport;
    QMessageBox *messageBox=new QMessageBox;
    QMediaPlayer *player=new QMediaPlayer;

};

#endif // ZHU_H
