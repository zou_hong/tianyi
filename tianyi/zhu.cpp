#include "zhu.h"
#include "ui_zhu.h"
#include<QPainter>
#include<QDebug>
#include<QTimer>
#include<QSerialPort>
#include<QDialog>
#include<QLabel>
#include<QMessageBox>
#include<QStandardItem>
#include<QSize>
#include <QTime>
#include<QMediaPlayer>

ZHU::ZHU(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ZHU)
{
    ui->setupUi(this);
/******************窗口显示*********************/
    this->setWindowFlags(Qt::Window);
    this->showFullScreen();
    this->setWindowFlags(Qt::FramelessWindowHint);
    //this->setAttribute(Qt::WA_DeleteOnClose);

    /******************设置按钮样式******************/
    ui->Buttonguomin->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/guomin1.png)}"
                                    "QPushButton:hover{border-image:url(:/new/prefix2/image/guomin2.png)}"
                                   "QPushButton:pressed{border-image:url(:/new/prefix2/image/guomin3.png) }");
    ui->Buttonguomin->setFlat(true);

    ui->Buttonfood->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/shipin1.png)}"
                                  "QPushButton:hover{border-image:url(:/new/prefix2/image/shipin2.png)}"
                                 "QPushButton:pressed{border-image:url(:/new/prefix2/image/shipin3.png) }");
    ui->Buttonfood->setFlat(true);

    ui->Buttondrink->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/yinpin1.png)}"
                                   "QPushButton:hover{border-image:url(:/new/prefix2/image/yinpin2.png)}"
                                  "QPushButton:pressed{border-image:url(:/new/prefix2/image/yinpin2.png) }");
    ui->Buttondrink->setFlat(true);

    ui->Buttonback->setStyleSheet("border-image:url(:/new/prefix2/image/back2.png)");
    ui->Buttonback->setFlat(true);

    /********************食品*********************/
    ui->Buttonkele->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/baichun1.png)}"
                                  "QPushButton:hover{border-image:url(:/new/prefix2/image/baichun2.png)}"
                                 "QPushButton:pressed{border-image:url(:/new/prefix2/image/baichun3.png) }");
    ui->pushButton2->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/bingan1.png)}"
                                                 "QPushButton:hover{border-image:url(:/new/prefix2/image/bingan2.png)}"
                                                "QPushButton:pressed{border-image:url(:/new/prefix2/image/bingan3.png) }");
    ui->pushButton3->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/dougan1.png)}"
                                                 "QPushButton:hover{border-image:url(:/new/prefix2/image/dougan2.png)}"
                                                "QPushButton:pressed{border-image:url(:/new/prefix2/image/dougan3.png) }");
    ui->pushButton4->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/leshi1.png)}"
                                                 "QPushButton:hover{border-image:url(:/new/prefix2/image/leshi2.png)}"
                                                "QPushButton:pressed{border-image:url(:/new/prefix2/image/leshi3.png) }");
    ui->pushButton5->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/shilijia1.png)}"
                                                 "QPushButton:hover{border-image:url(:/new/prefix2/image/shilijia2.png)}"
                                                "QPushButton:pressed{border-image:url(:/new/prefix2/image/shilijia3.png) }");
    ui->pushButton6->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/weilong1.png)}"
                                                 "QPushButton:hover{border-image:url(:/new/prefix2/image/weilong2.png)}"
                                                "QPushButton:pressed{border-image:url(:/new/prefix2/image/weilong3.png) }");

    /*********************饮品*******************/
    ui->pushButton7->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/kele1.png)}"
                                                 "QPushButton:hover{border-image:url(:/new/prefix1/image/kele2.png)}"
                                                "QPushButton:pressed{border-image:url(:/new/prefix1/image/kele3.png) }");
    ui->pushButton8->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/guoli1.png)}"
                                                 "QPushButton:hover{border-image:url(:/new/prefix1/image/guoli2.png)}"
                                                "QPushButton:pressed{border-image:url(:/new/prefix1/image/guoli3.png) }");
    ui->pushButton9->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/xuebi1.png)}"
                                                 "QPushButton:hover{border-image:url(:/new/prefix1/image/xuebi2.png)}"
                                                "QPushButton:pressed{border-image:url(:/new/prefix1/image/xuebi3.png) }");
    ui->pushButton10->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/nongfu1.png)}"
                                                 "QPushButton:hover{border-image:url(:/new/prefix1/image/nongfu2.png)}"
                                                "QPushButton:pressed{border-image:url(:/new/prefix1/image/nongfu3.png) }");
    ui->pushButton11->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/jianjiao1.png)}"
                                                 "QPushButton:hover{border-image:url(:/new/prefix1/image/jianjiao2.png)}"
                                                "QPushButton:pressed{border-image:url(:/new/prefix1/image/jianjiao3.png) }");
    ui->pushButton12->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/asamu1.png)}"
                                                 "QPushButton:hover{border-image:url(:/new/prefix1/image/asamu2.png)}"
                                                "QPushButton:pressed{border-image:url(:/new/prefix1/image/asamu3.png) }");
    //ui->Buttonjixu->setStyleSheet("QPushButton{border-image:url(:/image/jixu1.png)}"
                                                // "QPushButton:hover{border-image:url(:/image/jixu2.png)}"
                                                //"QPushButton:pressed{border-image:url(:/image/jixu1.png) }");

    ui->labelxinxi->setPixmap(QPixmap(":/new/prefix2/image/xinxi.png"));
    ui->labelzhifu->setPixmap(QPixmap(":/new/prefix2/image/zhifu.png"));
    ui->label->setPixmap(QPixmap(":/image/shuliang.png"));
    ui->labelguo->setPixmap(QPixmap(":/image/gmw.png"));
    ui->labelcant->setPixmap(QPixmap(":/image/cant.png"));
    ui->label_15->setPixmap(QPixmap(":/image/jin.png"));
    //付款码
    ui->pushButton->setStyleSheet("border-image:url(:/new/prefix2/image/fukuan.png)");
    //进度条
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->setValue(25);
    ui->progressBar->setFormat(QString("选择商品"));
    ui->progressBar->setAlignment(Qt::AlignRight);  // 对齐方式


    /*****************接收主窗口信号****************/

    connect(welcome,&Widget::open,this,&ZHU::dealopen);
    connect(welcome,&Widget::chinese,this,&ZHU::dealch);
    connect(welcome,&Widget::english,this,&ZHU::dealen);

/***********串口通信*************/
    //执行函数
     initPort();
/*******串口操作***************/
     connect(welcome,&Widget::shuaka1,this,&ZHU::jiance);
     connect(welcome,&Widget::color,this,&ZHU::juli_color);
     connect(welcome,&Widget::zhuzhou,this,&ZHU::zhuzhou);
     connect(welcome,&Widget::tuizi1,this,&ZHU::tuizi1);
     connect(welcome,&Widget::tuizi2,this,&ZHU::tuizi2);


/******获取数量*******/



}

ZHU::~ZHU()
{
    delete ui;
}
void ZHU::paintEvent(QPaintEvent *)
{
     //创建画家对象
    QPainter p(this);
    //绘图操作(背景图）
    p.drawPixmap(0,0,width(),height(),QPixmap(":/new/prefix2/image/bei.png"));
}

void ZHU::on_Buttonfood_clicked()
{
     ui->stackedWidget->setCurrentIndex(0);
}

void ZHU::on_Buttondrink_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void ZHU::on_Buttonguomin_clicked()
{
     ui->stackedWidget->setCurrentIndex(3);
}

void ZHU::on_pushButton1_clicked()
{
     ui->stackedWidget->setCurrentIndex(2);
}
//购买跳转：零食
void ZHU::on_Buttonkele_clicked()
{
    type = 1;//货物种类
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix2/image/baichun1.png"));
    ui->labelthing->setScaledContents(true);
    ui->progressBar->setValue(50);
    ui->progressBar->setFormat(QString("确认信息"));
    ui->progressBar->setAlignment(Qt::AlignRight);
    messageBox->about(this,"提示","过敏食物！");
}

void ZHU::on_pushButton2_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix2/image/bingan1.png"));
    ui->labelthing->setScaledContents(true);
}

void ZHU::on_pushButton3_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix2/image/dougan1.png"));
    ui->labelthing->setScaledContents(true);
}

void ZHU::on_pushButton4_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix2/image/leshi1.png"));
    ui->labelthing->setScaledContents(true);
}

void ZHU::on_pushButton5_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix2/image/shilijia1.png"));
    ui->labelthing->setScaledContents(true);
}

void ZHU::on_pushButton6_clicked()
{
    n=n-1;
    if(n==0){
        QMessageBox::about(this,"提示","该零食缺少货存！");
    }
    else{
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix2/image/weilong1.png"));
    ui->labelthing->setScaledContents(true);
    }
}

void ZHU::on_pushButton7_clicked()
{
    if(guoming == 1){
        QMessageBox::about(this,"警告","食物中含有过敏物质，勿购买！");
        player->setMedia(QUrl("qrc:/new/prefix1/music/guomingwu.mp3"));
        player->play();
    }
    else{
    type = 1;
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix1/image/kele1.png"));
    ui->labelthing->setScaledContents(true);}
}

void ZHU::on_pushButton8_clicked()
{
    type = 2;
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix1/image/guoli1.png"));
    ui->labelthing->setScaledContents(true);
}

void ZHU::on_pushButton9_clicked()
{
    if(guoming == 1){
        QMessageBox::about(this,"警告","食物中含有过敏物质，勿购买！");
        player->setMedia(QUrl("qrc:/new/prefix1/music/guomingwu.mp3"));
        player->play();
    }
    else{
    type = 3;
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix1/image/xuebi1.png"));
    ui->labelthing->setScaledContents(true);}
}

void ZHU::on_pushButton10_clicked()
{
    type = 4;
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix1/image/nongfu1.png"));
    ui->labelthing->setScaledContents(true);
}

void ZHU::on_pushButton11_clicked()
{
    type = 5;
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix1/image/jianjiao1.png"));
    ui->labelthing->setScaledContents(true);
}

void ZHU::on_pushButton12_clicked()
{
    type = 6;
    ui->stackedWidget->setCurrentIndex(2);
    ui->labelthing->setPixmap(QPixmap(":/new/prefix1/image/asamu1.png"));
    ui->labelthing->setScaledContents(true);
}

void ZHU::on_Buttonjixu_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->progressBar->setValue(25);
    ui->progressBar->setFormat(QString("选择商品"));
    ui->progressBar->setAlignment(Qt::AlignRight);
    amount= ui->spinBox->value();
    i=amount;
    j=type;;


}

//付款键
void ZHU::on_pushButton_clicked()
{
    ui->progressBar->setValue(75);
    ui->progressBar->setFormat(QString("付款中"));
    ui->progressBar->setAlignment(Qt::AlignRight);
    amount=ui->spinBox->value();
    qDebug()<<"选择的数量是："<<amount;
    if(i==0){
    switch(type){
    case 1:
        if(amount==1){
            QString str="07";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
        }
        else if(amount==2){
            QString str="07";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
            QString str1="07";
            QByteArray sendata1;
            StringToHex(str1,sendata1);
            //写入缓冲区
            my_serialport->write(sendata1);

        };break;
    case 2:
        if(amount==1){
            QString str="06";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
        }
        else if(amount==2){
            QString str="06";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
            QString str1="06";
            QByteArray sendata1;
            StringToHex(str1,sendata1);
            //写入缓冲区
            my_serialport->write(sendata1);
        };break;
    case 3:
        if(amount==1){
            QString str="09";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
        }
        else if(amount==2){
            QString str="09";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
            QString str1="09";
            QByteArray sendata1;
            StringToHex(str1,sendata1);
            //写入缓冲区
            my_serialport->write(sendata1);
        };break;
    case 4:
        if(amount==1){
            QString str="0A";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
        }
        else if(amount==2){
            QString str="0A";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
            QString str1="0A";
            QByteArray sendata1;
            StringToHex(str1,sendata1);
            //写入缓冲区
            my_serialport->write(sendata1);

        };break;
    case 5:
        if(amount==1){
            QString str="0B";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
        }
        else if(amount==2){
            QString str="0B";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
            QString str1="0B";
            QByteArray sendata1;
            StringToHex(str1,sendata1);
            //写入缓冲区
            my_serialport->write(sendata1);

        };break;
    case 6:
        if(amount==1){
            QString str="0C";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
        }
        else if(amount==2){
            QString str="0C";
            QByteArray sendata;
            StringToHex(str,sendata);
            //写入缓冲区
            my_serialport->write(sendata);
            QString str1="0C";
            QByteArray sendata1;
            StringToHex(str1,sendata1);
            //写入缓冲区
            my_serialport->write(sendata1);
        };break;
    }
   }
    else if(i==1){
        switch(j){
        case 1:
            if(i==1){
                QString str="07";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
                //第二种
                switch(type){
                case 1:
                    if(amount==1){
                        QString str="07";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                    }
                    else if(amount==2){
                        QString str="07";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                        QString str1="07";
                        QByteArray sendata1;
                        StringToHex(str1,sendata1);
                        //写入缓冲区
                        my_serialport->write(sendata1);

                    };break;
                case 2:
                    if(amount==1){
                        QString str="06";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                    }
                    else if(amount==2){
                        QString str="06";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                        QString str1="06";
                        QByteArray sendata1;
                        StringToHex(str1,sendata1);
                        //写入缓冲区
                        my_serialport->write(sendata1);
                    };break;
                case 3:
                    if(amount==1){
                        QString str="09";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                    }
                    else if(amount==2){
                        QString str="09";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                        QString str1="09";
                        QByteArray sendata1;
                        StringToHex(str1,sendata1);
                        //写入缓冲区
                        my_serialport->write(sendata1);
                    };break;
                case 4:
                    if(amount==1){
                        QString str="0A";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                    }
                    else if(amount==2){
                        QString str="0A";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                        QString str1="0A";
                        QByteArray sendata1;
                        StringToHex(str1,sendata1);
                        //写入缓冲区
                        my_serialport->write(sendata1);

                    };break;
                case 5:
                    if(amount==1){
                        QString str="0B";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                    }
                    else if(amount==2){
                        QString str="0B";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                        QString str1="0B";
                        QByteArray sendata1;
                        StringToHex(str1,sendata1);
                        //写入缓冲区
                        my_serialport->write(sendata1);

                    };break;
                case 6:
                    if(amount==1){
                        QString str="0C";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                    }
                    else if(amount==2){
                        QString str="0C";
                        QByteArray sendata;
                        StringToHex(str,sendata);
                        //写入缓冲区
                        my_serialport->write(sendata);
                        QString str1="0C";
                        QByteArray sendata1;
                        StringToHex(str1,sendata1);
                        //写入缓冲区
                        my_serialport->write(sendata1);
                    };break;
                }
            }
            else if(i==2){
                QString str="06";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
                QString str1="06";
                QByteArray sendata1;
                StringToHex(str1,sendata1);
                //写入缓冲区
                my_serialport->write(sendata1);

            };break;
        case 2:
            if(i==1){
                QString str="07";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
            }
            else if(i==2){
                QString str="07";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
                QString str1="07";
                QByteArray sendata1;
                StringToHex(str1,sendata1);
                //写入缓冲区
                my_serialport->write(sendata1);
            };break;
        case 3:
            if(i==1){
                QString str="08";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
            }
            else if(i==2){
                QString str="08";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
                QString str1="08";
                QByteArray sendata1;
                StringToHex(str1,sendata1);
                //写入缓冲区
                my_serialport->write(sendata1);
            };break;
        case 4:
            if(i==1){
                QString str="09";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
            }
            else if(i==2){
                QString str="09";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
                QString str1="09";
                QByteArray sendata1;
                StringToHex(str1,sendata1);
                //写入缓冲区
                my_serialport->write(sendata1);

            };break;
        case 5:
            if(i==1){
                QString str="010";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
            }
            else if(i==2){
                QString str="010";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
                QString str1="010";
                QByteArray sendata1;
                StringToHex(str1,sendata1);
                //写入缓冲区
                my_serialport->write(sendata1);

            };break;
        case 6:
            if(i==1){
                QString str="011";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
            }
            else if(i==2){
                QString str="011";
                QByteArray sendata;
                StringToHex(str,sendata);
                //写入缓冲区
                my_serialport->write(sendata);
                QString str1="011";
                QByteArray sendata1;
                StringToHex(str1,sendata1);
                //写入缓冲区
                my_serialport->write(sendata1);

            };break;
        }

    }
}

void ZHU::dealch(){
    /******************设置按钮样式******************/
    ui->Buttonguomin->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/guomin1.png)}"
                                    "QPushButton:hover{border-image:url(:/new/prefix2/image/guomin2.png)}"
                                   "QPushButton:pressed{border-image:url(:/new/prefix2/image/guomin3.png) }");
    ui->Buttonguomin->setFlat(true);

    ui->Buttonfood->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/shipin1.png)}"
                                  "QPushButton:hover{border-image:url(:/new/prefix2/image/shipin2.png)}"
                                 "QPushButton:pressed{border-image:url(:/new/prefix2/image/shipin3.png) }");
    ui->Buttonfood->setFlat(true);

    ui->Buttondrink->setStyleSheet("QPushButton{border-image:url(:/new/prefix2/image/yinpin1.png)}"
                                   "QPushButton:hover{border-image:url(:/new/prefix2/image/yinpin2.png)}"
                                  "QPushButton:pressed{border-image:url(:/new/prefix2/image/yinpin2.png) }");
    ui->Buttondrink->setFlat(true);

    ui->Buttonback->setStyleSheet("border-image:url(:/new/prefix2/image/back2.png)");
    ui->Buttonback->setFlat(true);
    ui->labelxinxi->setPixmap(QPixmap(":/new/prefix2/image/xinxi.png"));
    ui->labelzhifu->setPixmap(QPixmap(":/new/prefix2/image/zhifu.png"));
    ui->Buttonjixu->setText("继续购买");
    ui->labelguo->setPixmap(QPixmap(":/image/gmw.png"));
    ui->labelcant->setPixmap(QPixmap(":/image/cant.png"));

}


void ZHU::dealen(){
    /******************设置按钮样式******************/
    ui->Buttonguomin->setStyleSheet("border-image:url(:/new/prefix4/image/allergy.png)");

    ui->Buttonguomin->setFlat(true);

    ui->Buttonfood->setStyleSheet("border-image:url(:/new/prefix4/image/snacks.png)");

    ui->Buttonfood->setFlat(true);

    ui->Buttondrink->setStyleSheet("border-image:url(:/new/prefix4/image/drink.png)");

    ui->Buttondrink->setFlat(true);

    ui->Buttonback->setStyleSheet("border-image:url(:/new/prefix4/image/back1.png)");
    ui->Buttonback->setFlat(true);
    ui->labelxinxi->setPixmap(QPixmap(":/new/prefix4/image/information.png"));
    ui->labelzhifu->setPixmap(QPixmap(":/new/prefix4/image/payment.png"));
    ui->Buttonjixu->setText("go on");
    ui->labelguo->setPixmap(QPixmap(":/image/gmw2.png"));
    ui->labelcant->setPixmap(QPixmap(":/image/cant2.png"));
}

void ZHU::dealopen(){
    this->show();
    //qDebug()<<"chenggob";

}

void ZHU::on_Buttonback_clicked()
{
    //this->close();
    welcome->show();
    QString str="0D";
    QByteArray sendata;
    StringToHex(str,sendata);
    //写入缓冲区
    my_serialport->write(sendata);
    qDebug()<<"开始监测信息卡";

}


/***************串口通信****************/
/***********************串口初始化*****************************/
void ZHU::initPort()
{
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
       {
           qDebug()<<"Name:"<<info.portName();
           qDebug()<<"Description:"<<info.description();
           qDebug()<<"Manufacturer:"<<info.manufacturer();

           //这里相当于自动识别串口号之后添加到了cmb，如果要手动选择可以用下面列表的方式添加进去
           QSerialPort serial;
           serial.setPort(info);
           if(serial.open(QIODevice::ReadWrite))
                   {
                       //将串口号添加到cmb
                       ui->cmbPortName->addItem(info.portName());
                       //关闭串口等待人为(打开串口按钮)打开
                       serial.close();
                   }
               }

 }

/****************************串口设置******************************/





void ZHU::on_btnOpen_clicked()
{
    ui->btnOpen->hide();
    ui->cmbPortName->hide();
    if(ui->btnOpen->text() == "打开串口")
       {
           my_serialport = new QSerialPort(this);

           //设置串口号
           my_serialport->setPortName(ui->cmbPortName->currentText());
           //以读写方式打开串口
           if(my_serialport->open(QIODevice::ReadWrite))
           {

               //设置波特率
               my_serialport->setBaudRate(115200);
               //设置数据位
               my_serialport->setDataBits(QSerialPort::Data8);
               //设置校验位
               my_serialport->setParity(QSerialPort::NoParity);
               //设置流控制
               my_serialport->setFlowControl(QSerialPort::NoFlowControl);
               //设置停止位
               my_serialport->setStopBits(QSerialPort::OneStop);

               //每秒读一次
               timer = new QTimer(this);
               connect(timer, SIGNAL(timeout()), this, SLOT(readComDataSlot()));

               timer->start(1000);
               qDebug()<<"打开串口成功";


           }
           else
           {
               QMessageBox::about(NULL, "提示", "串口没有打开！");
               return;
           }
       }
       else
       {
           timer->stop();
            //QStandardItem::setSelectable;
           my_serialport->close();
       }
}
/****************************数据传输******************************/
void ZHU::readComDataSlot()
{
    //读取串口数据
    QByteArray readComData = my_serialport->readAll();
    QString q;
    q=QString(readComData);
    qDebug()<<q;
    if(q == "1")
    {
        player->setMedia(QUrl("qrc:/new/prefix1/music/shukachenggong.mp3"));
        player->play();
        ui->textEdit->setText("碳酸");
        ui->label_gm1->setPixmap(QPixmap(":/new/prefix1/image/kele1.png"));
        ui->label_gm2->setPixmap(QPixmap(":/new/prefix1/image/xuebi1.png"));
        ui->label_gm3->setPixmap(QPixmap(":/new/prefix2/image/baichun1.png"));
        guoming=1;
        welcome->close();
    }
    if(q == "3")
    {
        player->setMedia(QUrl("qrc:/new/prefix1/music/shukachenggong.mp3"));
        player->play();
        ui->textEdit->setText("糖分");
        ui->label_gm1->setPixmap(QPixmap(":/new/prefix2/image/bingan1.png"));
        ui->label_gm2->setPixmap(QPixmap(":/new/prefix2/image/shilijia1.png"));
        ui->label_gm3->setPixmap(QPixmap(":/new/prefix1/image/asamu1.png"));
        guoming=3;
        welcome->close();
    }
    if(q == "2")
    {
        player->setMedia(QUrl("qrc:/new/prefix1/music/shukachenggong.mp3"));
        player->play();
        ui->textEdit->setText("纤维");
        ui->label_gm1->setPixmap(QPixmap(":/new/prefix1/image/guoli1.png"));
        ui->label_gm2->setPixmap(QPixmap(":/new/prefix2/image/dougan1.png"));
        ui->label_gm3->setPixmap(QPixmap(":/new/prefix1/image/jianjiao1.png"));
        guoming  =2;
        welcome->close();
    }
    if(q == "4")
    {
        player->setMedia(QUrl("qrc:/new/prefix1/music/shukachenggong.mp3"));
        player->play();
        ui->textEdit->setText("杂质");
        ui->label_gm1->setPixmap(QPixmap(":/new/prefix1/image/nongfu1.png"));
        ui->label_gm2->setPixmap(QPixmap(":/new/prefix2/image/weilong1.png"));
        ui->label_gm3->setPixmap(QPixmap(":/new/prefix2/image/leshi1.png"));
        guoming = 4;
        welcome->close();
    }
    if(q == "5")
    {
        QMessageBox::about(NULL, "提示", "距离过近，请远离售货机");
    }
    if(q == "6666")
    {
        QMessageBox::about(NULL, "提示", "上货成功！");
    }
    if(q == "6")
    {
        QMessageBox::about(NULL, "提示", "上货完毕！");
    }
    if(q == "7")
    {
        QMessageBox::about(NULL, "提示", "出货成功");
    }
    //清除缓冲区
    readComData.clear();
}


void  ZHU::StringToHex(QString str, QByteArray & senddata)  //字符串转换成16进制数据0-F
{
    int hexdata,lowhexdata;
    int hexdatalen = 0;
    int len = str.length();
    senddata.resize(len/2);
    char lstr,hstr;
    for(int i=0; i<len; )
    {
        //char lstr,
        hstr=str[i].toLatin1();
        if(hstr == ' ')
        {
            i++;
            continue;
        }
        i++;
        if(i >= len)
            break;
        lstr = str[i].toLatin1();
        hexdata = ConvertHexChar(hstr);
        lowhexdata = ConvertHexChar(lstr);
        if((hexdata == 16) || (lowhexdata == 16))
            break;
        else
            hexdata = hexdata*16+lowhexdata;
        i++;
        senddata[hexdatalen] = (char)hexdata;
        hexdatalen++;
    }
    senddata.resize(hexdatalen);
}

char ZHU::ConvertHexChar(char ch)
{
    if((ch >= '0') && (ch <= '9'))
            return ch-0x30;
    else if((ch >= 'A') && (ch <= 'F'))
            return ch-'A'+10;
    else if((ch >= 'a') && (ch <= 'f'))
            return ch-'a'+10;
//        else return (-1);
    else return ch-ch;//不在0-f范围内的会发送成0
}

//串口操作
void ZHU::jiance(){     //开始检测信息卡
    QString str="0D";
    QByteArray sendata;
    StringToHex(str,sendata);
    //写入缓冲区
    my_serialport->write(sendata);
    qDebug()<<"开始监测信息卡";

}



void ZHU::on_pushButton_2_clicked()  //开始上货
{
    ui->pushButton_2->hide();
    QString str="01";
    QByteArray sendata;
    StringToHex(str,sendata);
    //写入缓冲区
    my_serialport->write(sendata);
    qDebug()<<"上货成功";
}


//维护反应
void ZHU::juli_color(){
    qDebug()<<"检测距离-颜色传感器";
    QString str="02";
    QByteArray sendata;
    StringToHex(str,sendata);
    //写入缓冲区
    my_serialport->write(sendata);
    QByteArray readComData = my_serialport->readAll();
    QString q;
    q=QString(readComData);
    if(q !=" ")
        QMessageBox::about(NULL, "提示", "功能正常！");

}

void ZHU::zhuzhou(){
    qDebug()<<"检测主轴";
    QString str="03";
    QByteArray sendata;
    StringToHex(str,sendata);
    //写入缓冲区
    my_serialport->write(sendata);
    //sleep(2500);
    /*QString str1="014";
    QByteArray sendata1;
    StringToHex(str1,sendata1);
    //写入缓冲区
    my_serialport->write(sendata1);*/
}

void ZHU::tuizi1(){
    qDebug()<<"检测推子1";
    QString str="04";
    QByteArray sendata;
    StringToHex(str,sendata);
    //写入缓冲区
    my_serialport->write(sendata);

}

void ZHU::tuizi2(){
    qDebug()<<"检测推子2";
    QString str="05";
    QByteArray sendata;
    StringToHex(str,sendata);
    //写入缓冲区
    my_serialport->write(sendata);
}

//延时函数
void ZHU::sleep(unsigned int msec)
{
    QTime dieTime = QTime::currentTime().addMSecs(msec);
    while( QTime::currentTime() < dieTime )
    QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}
