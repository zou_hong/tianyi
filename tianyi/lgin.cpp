#include "lgin.h"
#include "ui_lgin.h"
#include <QMessageBox>

Lgin::Lgin(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Lgin)
{
    ui->setupUi(this);
    this->setWindowTitle("维护人员登录");
     ui->label->setPixmap(QPixmap(":/new/prefix3/image/WEIHUREUYUAN.png"));
     ui->label->setScaledContents(true);
     ui->lineEdit->setEchoMode(QLineEdit::Password);
     ui->pushButton->setStyleSheet("background:rgb(85, 170, 255);font: 75 20pt  ADMUI3Lg;border-radius:10px;border:2px solid white;color:white");
     ui->label_4->setPixmap(QPixmap(":/new/prefix3/image/beijin.png"));
     ui->label_4->setScaledContents(true);//自适应大小
     ui->Buttonjuli->setStyleSheet("border-image:url(:/new/prefix3/image/tiaoshi.png)");
     ui->Buttontui_1->setStyleSheet("border-image:url(:/new/prefix3/image/tiaoshi.png)");
     ui->Buttontui_2->setStyleSheet("border-image:url(:/new/prefix3/image/tiaoshi.png)");
     ui->Buttonzhuzhou->setStyleSheet("border-image:url(:/new/prefix3/image/tiaoshi.png)");
}

Lgin::~Lgin()
{
    delete ui;
}

void Lgin::on_pushButton_clicked()
{
    QString str=ui->lineEdit->text();
    int b;
    b=str.toInt();
    if(123456==b)
    {
        ui->lineEdit->clear();
        ui->stackedWidget->setCurrentIndex(1);
    }
   else
    {
        ui->label_3->setText("维修密码错误!");
    }
}

void Lgin::on_pushButton_2_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
    this->close();
}

void Lgin::on_pushButton_3_clicked()
{
    QMessageBox::information(this,"提示","测试中，请稍后。");
}

void Lgin::on_Buttonjuli_clicked()
{
    emit color();
}

void Lgin::on_Buttonzhuzhou_clicked()
{
    emit zhuzhou();
}

void Lgin::on_Buttontui_1_clicked()
{
    emit tuizi1();
}

void Lgin::on_Buttontui_2_clicked()
{
    emit tuizi2();
}
