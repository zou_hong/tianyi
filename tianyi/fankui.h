#ifndef FANKUI_H
#define FANKUI_H

#include <QWidget>
#include<QUdpSocket>//UDP套接字

namespace Ui {
class Fankui;
}

class Fankui : public QWidget
{
    Q_OBJECT

public:
    explicit Fankui(QWidget *parent = 0);
    ~Fankui();
    QUdpSocket *udpsocket=new QUdpSocket;//UDP套接字
    QString strScrollCation = QString("很抱歉给您带来了困扰，如遇到以下问题请及时发送给我们，我们会及时处理！");

protected:
    //绘图事件
    void paintEvent(QPaintEvent*);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void scrollCaption();

    void on_pushButton_5_clicked();

private:
    Ui::Fankui *ui;
};

#endif // FANKUI_H
