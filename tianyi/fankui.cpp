#include "fankui.h"
#include "ui_fankui.h"
#include<QTimer>
#include<QHostAddress>
#include <QPainter>
#include <QPixmap>
#include <QMovie>
#include <QMessageBox>


Fankui::Fankui(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Fankui)
{
    ui->setupUi(this);
    this->setWindowTitle("用户反馈界面");
    udpsocket->bind(0002);

    QMovie *mymovie=new QMovie(":/new/prefix3/image/11111.gif");//创建动画
    ui->labelgif->setMovie(mymovie);//设置动画
    mymovie->start();//启动动画
    ui->labelgif->setScaledContents(true);//自适应大小


/*******************跑马字幕*************************/
    QTimer *pTimer = new QTimer(this);
    connect(pTimer,  SIGNAL(timeout()),  this,  SLOT(scrollCaption()));
    pTimer->start(400);
    //ui->labelad->setStyleSheet("border:6px solid rgb(254,223,186)");


}

Fankui::~Fankui()
{
    delete ui;
}

void Fankui::on_pushButton_clicked()
{
    QString ip=ui->lineEditip->text();
    qint16 port=ui->lineEditport->text().toInt();
    //获取编辑区内容
    QString str = ui->textEdit->toPlainText();

    //给指定IP发送数据
    udpsocket->writeDatagram(str.toUtf8(),QHostAddress(ip),port);
    QMessageBox::information(this,"提示","反馈成功，感谢！");
}
/*************绘制背景****************/
void Fankui::paintEvent(QPaintEvent *)
{
     //创建画家对象
    QPainter p(this);
    //绘图操作(背景图）
    p.drawPixmap(0,0,width(),height(),QPixmap(":/new/prefix3/image/fankui.png"));
}


void Fankui::on_pushButton_2_clicked()
{
    QString ip=ui->lineEditip->text();
    qint16 port=ui->lineEditport->text().toInt();
    //获取编辑区内容
    QString str = ui->textEdit_2->toPlainText();

    //给指定IP发送数据
    udpsocket->writeDatagram(str.toUtf8(),QHostAddress(ip),port);
    QMessageBox::information(this,"提示","反馈成功，感谢！");
}

void Fankui::on_pushButton_3_clicked()
{
    QString ip=ui->lineEditip->text();
    qint16 port=ui->lineEditport->text().toInt();
    //获取编辑区内容
    QString str = ui->textEdit_3->toPlainText();

    //给指定IP发送数据
    udpsocket->writeDatagram(str.toUtf8(),QHostAddress(ip),port);
    QMessageBox::information(this,"提示","反馈成功，感谢！");
}

void Fankui::on_pushButton_4_clicked()
{
    QString ip=ui->lineEditip->text();
    qint16 port=ui->lineEditport->text().toInt();
    //获取编辑区内容
    QString str = ui->textEdit_4->toPlainText();

    //给指定IP发送数据
    udpsocket->writeDatagram(str.toUtf8(),QHostAddress(ip),port);
    QMessageBox::information(this,"提示","反馈成功，感谢！");
}
void Fankui::scrollCaption()
{
    static int nPos = 0;

    // 当截取的位置比字符串长时，从头开始
    if (nPos > strScrollCation.length())
        nPos = 0;

    ui->labelpaoma->setText(strScrollCation.mid(nPos));
    nPos++;
}

void Fankui::on_pushButton_5_clicked()
{
    QString ip=ui->lineEditip->text();
    qint16 port=ui->lineEditport->text().toInt();
    //获取编辑区内容
    QString str = ui->textEditfankui->toPlainText();

    //给指定IP发送数据
    udpsocket->writeDatagram(str.toUtf8(),QHostAddress(ip),port);
    QMessageBox::information(this,"提示","反馈成功，感谢！");
}
