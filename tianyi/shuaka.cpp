#include "shuaka.h"
#include "ui_shuaka.h"
#include <QMovie>


SHUAKA::SHUAKA(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SHUAKA)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::FramelessWindowHint);
    /***********************设置背景**********************/
    QMovie *mymovie=new QMovie(":/new/prefix1/image/LOADING.gif");//创建动画
    ui->labelGIF->setMovie(mymovie);//设置动画
    mymovie->start();//启动动画
    ui->labelGIF->setScaledContents(true);//自适应大小
    /*********************设置提示********************/

    ui->labeltishi->setPixmap(QPixmap(":/new/prefix1/image/tishi.png"));
    ui->pushButton->setStyleSheet("border-image:url(:/new/prefix1/image/close.png)");

    ui->Buttonsucc->setFlat(true);


    /************************建立槽函数****************/
    connect(ui->Buttonsucc,QPushButton::clicked,this,SHUAKA::send);
    emit shuaka();
    qDebug()<<"fasongxihao";
}

SHUAKA::~SHUAKA()
{
    delete ui;
}

void SHUAKA::on_Buttonsucc_clicked()
{
    ui->labeltishi->setPixmap(QPixmap(":/new/prefix1/image/tishi.png"));
    //ui->labelzhuangtai->hide();


}

void SHUAKA::on_pushButton_clicked()
{
    this->close();

}

void SHUAKA::send()
{
    emit opensignal();
}
