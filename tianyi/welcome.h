#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include<QMediaPlayer>
#include<QUdpSocket>//UDP套接字
#include "shizhong.h"
#include"shuaka.h"
//#include"zhu.h"
#include"lgin.h"
#include "fankui.h"
#include"html.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
     QString strScrollCation = QString(" 今天全场零食五折起，赶快来抢购吧！");
     QUdpSocket *udpsocket=new QUdpSocket;//UDP套接字
     void dealmeg();
     void sentch();
     void senten();
     static int i;

protected:
    //绘图事件
    void paintEvent(QPaintEvent*);

signals:
    void chinese();
    void english();
    void open();
    void shuaka1();//提示刷卡信号

    //维护信号
    void color();
    void zhuzhou();
    void tuizi1();
    void tuizi2();


private slots:
    void on_Buttonmusic_clicked(bool checked);

    void on_Buttonbuy_clicked();

    void deal();

    void scrollCaption();


    void on_Buttonsetup_clicked();

    void on_Buttonproblem_clicked();

    void on_comboBox_currentIndexChanged(int index);

    void on_Buttonabout_clicked();

    void chu_shua();

    //维护通信
    void chu_color();
    void chu_zhuzhou();
    void chu_tuizi1();
    void chu_tuizi2();

private:
    Ui::Widget *ui;
    QMediaPlayer *player=new QMediaPlayer;
    MainWindow *shizhong=new MainWindow;
    SHUAKA *shua=new SHUAKA;
    //ZHU *zhu=new ZHU;
    Lgin *lgin=new Lgin;
    Fankui *fankui =new Fankui;
    Html *html =new Html;
    QMediaPlayer *player_q=new QMediaPlayer;
    QMediaPlayer *player_p=new QMediaPlayer;
};

#endif // WIDGET_H
