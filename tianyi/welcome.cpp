#include "welcome.h"
#include "ui_widget.h"
#include <QMovie>
#include <QPainter>
#include <QLabel>
#include <QPixmap>
#include <QIcon>
#include <QDialog>
#include<QMediaPlayer>
#include<QTimer>
#include<QHostAddress>
#include <QDebug>



Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
/****************窗口操作去掉标题栏，窗口最大化********************/
    //去掉标题栏
    this->setWindowFlags(Qt::Window);
    this->setWindowFlags(Qt::FramelessWindowHint);
    //全屏显示
    this->showFullScreen();
    //设置logo
    //设置logo
    ui->labellogo->setPixmap(QPixmap(":/new/prefix1/image/logo3.png"));

/****************设置按钮图标**********************/
    ui->Buttonbuy->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/nowbuy2.png)}"
                                  "QPushButton:hover{border-image:url(:/new/prefix1/image/nowbuy4.png)}"
                                 "QPushButton:pressed{border-image:url(:/new/prefix1/image/nowbuy3.png) }");
    //ui->pushButton->setIcon(QIcon(":/new/prefix1/image/to buy.png"));
    //ui->pushButton->setIconSize(QSize(500,200));
    //设置按键透明
    ui->Buttonbuy->setFlat(true);
    //同理设置按键二
    //设置按键图片
    ui->Buttonabout->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/about.png)}"
                                    "QPushButton:hover{border-image:url(:/new/prefix1/image/about1.png)}"
                                   "QPushButton:pressed{border-image:url(:/new/prefix1/image/about2.png) }");
    //ui->pushButton_2->setIcon(QIcon(":/new/prefix1/image/about us.png"));
    //ui->pushButton_2->setIconSize(QSize(500,200));
    ui->Buttonabout->setFlat(true);//设置按键透明
    ui->Buttonproblem->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/wenti1.png)}"
                                    "QPushButton:hover{border-image:url(:/new/prefix1/image/wenti2.png)}"
                                   "QPushButton:pressed{border-image:url(:/new/prefix1/image/wenti3.png) }");
    ui->Buttonproblem->setFlat(true);
    ui->Buttonsetup->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/setup.png)}"
                                   "QPushButton:hover{border-image:url(:/new/prefix1/image/setup2.png)}"
                                  "QPushButton:pressed{border-image:url(:/new/prefix1/image/setup3.png) }");//维护键

/******************播放音乐**************/
    player->setMedia(QUrl("qrc:/new/prefix1/music/guanggao.mp3"));
    ui->Buttonmusic->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/start.png)}"
                                   "QPushButton:hover{border-image:url(:/new/prefix1/image/star2.png)}"
                                  "QPushButton:pressed{border-image:url(:/new/prefix1/image/start3.png) }");

/*******************弹幕*********************/

    ui->labellaba->setPixmap(QPixmap(":/new/prefix1/image/laba.png"));//设置logo

/**************************设置广告页********************/

    QMovie *mymovie=new QMovie(":/new/prefix1/image/ad.gif");//创建动画
    ui->labelad->setMovie(mymovie);//设置动画
    mymovie->start();//启动动画
    ui->labelad->setScaledContents(true);//自适应大小
    ui->labelad->setStyleSheet("border:6px solid rgb(254,223,186)");
    QMovie *mymovie1=new QMovie(":/new/prefix1/image/movie2.gif");//创建动画
    ui->labelGIF->setMovie(mymovie1);//设置动画
    mymovie1->start();//启动动画
    ui->labelGIF->setScaledContents(true);//自适应大小
    ui->labelGIF->setStyleSheet("border:6px solid rgb(254,223,186)");

/***************************双语*******************/
    ui->labellanguage->setPixmap(QPixmap(":/new/prefix1/image/language.png"));


/*******************8显示子窗口****************/
    //shizhong->showNormal();

    // shizhong->setGeometry(1680,0,245,230);

/******************打开主界面************************/
    connect(shua,&SHUAKA::opensignal,this,&Widget::deal);


/*******************跑马字幕*************************/
    QTimer *pTimer = new QTimer(this);
    connect(pTimer,  SIGNAL(timeout()),  this,  SLOT(scrollCaption()));
    pTimer->start(400);
    ui->labelad->setStyleSheet("border:6px solid rgb(254,223,186)");


/********************UDP通信***************************/
    udpsocket->bind(0001);//端口值
    connect(udpsocket,&QUdpSocket::readyRead,this,&Widget::dealmeg);

/******************下拉按钮样式表******************/
    ui->comboBox->setStyleSheet("QComboBox{border:1px solid #d7d7d7; border-radius: 3px; padding: 1px 18px 1px 3px;} "
                              "QComboBox:editable{ background: white; }"
                              "QComboBox:!editable{ background: #fbfbfb; color:#666666}"
                              "QComboBox::drop-down{ subcontrol-origin: padding; subcontrol-position: top right; width: 22px; border-left-width: 1px; border-left-color: #c9c9c9; border-left-style: solid; /* just a single line */ border-top-right-radius: 3px; /* same radius as the QComboBox */ border-bottom-right-radius: 3px; }"
                              "QComboBox::down-arrow { image: url(:/new/prefix1/image/xiam.png); }"
                              "QComboBox::down-arrow:on { /* shift the arrow when popup is open */ top: 1px; left: 1px;}"
                              "QComboBox QAbstractItemView::item{max-width: 30px;min-height: 20px}");

/************处理维护信息*********/
    connect(shua,&SHUAKA::shuaka,this,&Widget::chu_shua);

    connect(lgin,&Lgin::color,this,&Widget::chu_color);
    connect(lgin,&Lgin::zhuzhou,this,&Widget::chu_zhuzhou);
    connect(lgin,&Lgin::tuizi1,this,&Widget::chu_tuizi1);
    connect(lgin,&Lgin::tuizi2,this,&Widget::chu_tuizi2);




}

Widget::~Widget()
{
    delete ui;
}
/*************绘制背景****************/
void Widget::paintEvent(QPaintEvent *)
{
     //创建画家对象
    QPainter p(this);
    //绘图操作(背景图）
    p.drawPixmap(0,0,width(),height(),QPixmap(":/new/prefix1/image/di.png"));
}

/**************播放************/
void Widget::on_Buttonmusic_clicked(bool checked)
{
    ui->Buttonmusic->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/stop.png)}"
                                   "QPushButton:hover{border-image:url(:/new/prefix1/image/stop2.png)}"
                                  "QPushButton:pressed{border-image:url(:/new/prefix1/image/stop3.png) }");


   player->play();

   if(!checked){   ui->Buttonmusic->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/start.png)}"
                                                  "QPushButton:hover{border-image:url(:/new/prefix1/image/star2.png)}"
                                                 "QPushButton:pressed{border-image:url(:/new/prefix1/image/start3.png) }");


   player->stop();
   }
}

/************************按键功能实现****************/

void Widget::on_Buttonbuy_clicked()
{

    shua->setModal(true);
    shua->show();
    player_q->setMedia(QUrl("qrc:/new/prefix1/music/qingshuaka.mp3"));
    player_p->setMedia(QUrl("qrc:/new/prefix1/music/sendcard.mp3"));
    //emit shuaka();//发送开始检测信息卡信号
    if (ui->comboBox->currentIndex()==0)
    {
        player_q->play();
    }
    if (ui->comboBox->currentIndex()==1)
    {
        player_p->play();
    }

}


//打开主界面
void Widget::deal()
{
    shua->close();
    //qDebug()<<"chenggong";
    emit open();
    this->close();
    emit open();
}

//字幕
void Widget::scrollCaption()
{
    static int nPos = 0;

    // 当截取的位置比字符串长时，从头开始
    if (nPos > strScrollCation.length())
        nPos = 0;

    ui->labeldanmu->setText(strScrollCation.mid(nPos));
    nPos++;
}

//召唤维修界面
void Widget::on_Buttonsetup_clicked()
{

    lgin->show();
}

//UDP
void Widget::dealmeg()
{
    //读取对方发送的内容
    char buf[1024]={0};
    quint16 port;//对方端口
    QHostAddress cliadd;
    qint64 len= udpsocket->readDatagram(buf,sizeof(buf),&cliadd,&port);
    if(len > 0)
    {
        //格式化
         strScrollCation = QString("%1").arg(buf);
        //编辑区给内容
        //ui->label->setText(strScrollCation);
    }

}

//召唤反馈页面
void Widget::on_Buttonproblem_clicked()
{
    fankui->show();
}

void Widget::on_comboBox_currentIndexChanged(int index)
{
    if(index==0)
    {
        ui->Buttonbuy->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/nowbuy2.png)}"
                                      "QPushButton:hover{border-image:url(:/new/prefix1/image/nowbuy4.png)}"
                                     "QPushButton:pressed{border-image:url(:/new/prefix1/image/nowbuy3.png) }");
        //ui->pushButton->setIcon(QIcon(":/new/prefix1/image/to buy.png"));
        //ui->pushButton->setIconSize(QSize(500,200));
        //设置按键透明
        ui->Buttonbuy->setFlat(true);
        //同理设置按键二
        //设置按键图片
        ui->Buttonabout->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/about.png)}"
                                        "QPushButton:hover{border-image:url(:/new/prefix1/image/about1.png)}"
                                       "QPushButton:pressed{border-image:url(:/new/prefix1/image/about2.png) }");
        //ui->pushButton_2->setIcon(QIcon(":/new/prefix1/image/about us.png"));
        //ui->pushButton_2->setIconSize(QSize(500,200));
        ui->Buttonabout->setFlat(true);//设置按键透明
        ui->Buttonproblem->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/wenti1.png)}"
                                        "QPushButton:hover{border-image:url(:/new/prefix1/image/wenti2.png)}"
                                       "QPushButton:pressed{border-image:url(:/new/prefix1/image/wenti3.png) }");
        ui->Buttonproblem->setFlat(true);

       //ui->Buttonsetup->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/setup.png)}"
                                       //"QPushButton:hover{border-image:url(:/new/prefix1/image/setup2.png)}"
                                      //"QPushButton:pressed{border-image:url(:/new/prefix1/image/setup3.png) }");//维护键
       sentch();
       strScrollCation = QString("今天全场零食五折起，赶快来抢购吧！");
        //zhu->actions();

    }
    if(index==1)
    {
        ui->Buttonbuy->setStyleSheet("QPushButton{border-image:url(:/new/prefix4/image/BUYNOW.png)}"
                                      "QPushButton:hover{border-image:url(:/new/prefix4/image/BUYNOW2.png)}"
                                     "QPushButton:pressed{border-image:url(:/new/prefix4/image/BUYNOW3.png) }");
        //ui->pushButton->setIcon(QIcon(":/new/prefix1/image/to buy.png"));
        //ui->pushButton->setIconSize(QSize(500,200));
        //设置按键透明
        ui->Buttonbuy->setFlat(true);
        //同理设置按键二
        //设置按键图片
        ui->Buttonabout->setStyleSheet("QPushButton{border-image:url(:/new/prefix4/image/ABOUTUS.png)}"
                                        "QPushButton:hover{border-image:url(:/new/prefix4/image/ABOUTUS2.png)}"
                                       "QPushButton:pressed{border-image:url(:/new/prefix4/image/ABOUTUS3.png) }");
        //ui->pushButton_2->setIcon(QIcon(":/new/prefix1/image/about us.png"));
        //ui->pushButton_2->setIconSize(QSize(500,200));
        ui->Buttonabout->setFlat(true);//设置按键透明
        ui->Buttonproblem->setStyleSheet("QPushButton{border-image:url(:/new/prefix4/image/FEEDBACK1.png)}"
                                        "QPushButton:hover{border-image:url(:/new/prefix4/image/FEEDBACK2.png)}"
                                       "QPushButton:pressed{border-image:url(:/new/prefix4/image/FEEDBACK3.png) }");
        ui->Buttonproblem->setFlat(true);
        //ui->Buttonsetup->setStyleSheet("QPushButton{border-image:url(:/new/prefix1/image/setup.png)}"
                                      // "QPushButton:hover{border-image:url(:/new/prefix1/image/setup2.png)}"
                                      //"QPushButton:pressed{border-image:url(:/new/prefix1/image/setup3.png) }");//维护键
        senten();
        strScrollCation = QString("  Today the snacks five fold！");



    }
}

//发送信号
void Widget::sentch(){
    emit chinese();
}

void Widget::senten(){
    emit english();
}


void Widget::on_Buttonabout_clicked()
{

   html->show();
}
//维护信号
void Widget::chu_color(){
    emit color();
}

void Widget::chu_zhuzhou(){
    emit zhuzhou();
}

void Widget::chu_tuizi1(){
    emit tuizi1();
}

void  Widget::chu_tuizi2(){
    emit tuizi2();
}

void Widget::chu_shua(){

    qDebug()<<"传递刷卡信息";
    emit shuaka1();//发送开始检测信息卡信号
}
//延时函数


