#include "html.h"
#include "ui_html.h"
#include <QWebView>

Html::Html(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Html)
{
    ui->setupUi(this);
    setWindowTitle("天一官方网站");
    ui->webView->load(QUrl("qrc:/new/prefix2/HTML/btzero_17_Smallapps/index.html"));
    //ui->webView->setHtml("<a href=";#"><img src="file:///img/logo.png" alt="logo"></a>");
    ui->webView->show();
}

Html::~Html()
{
    delete ui;
}
